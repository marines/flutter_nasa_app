import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'models/n_e_o_with_distance.dart';
import 'models/near_earth_object.dart';
import 'models/near_earth_object_response_dto.dart';
import 'widgets/date_picker.dart';
import 'widgets/earth_row.dart';
import 'widgets/n_e_o_row.dart';

class EarthPage extends StatefulWidget {
  const EarthPage({super.key});

  @override
  _EarthPageState createState() => _EarthPageState();
}

class _EarthPageState extends State<EarthPage> {
  final TextEditingController _dateController = TextEditingController(
    text: DateFormat('yyyy-MM-dd').format(DateTime.now()),
  );

  List<NearEarthObject> neos = [];
  int maxDistance = 0;

  @override
  void initState() {
    super.initState();
    fetchImages();
  }

  @override
  Widget build(BuildContext context) {
    final List<NEOWithDistance> topObjects = [];
    final List<NEOWithDistance> bottomObjects = [];
    final List<NearEarthObject> neos = [...this.neos];

    if (neos.length > 1) {
      splitNeos(neos, topObjects, bottomObjects);
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Asteroids Today'),
        actions: [
          DatePicker(
            dateController: _dateController,
            onSelected: (date) {
              fetchImages();
            },
            context: context,
          ),
        ],
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/space.gif'),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ...topObjects.reversed.map(
                    (neoWithDistance) => NEORow(
                      neoWithDistance: neoWithDistance,
                      maxDistance: maxDistance,
                      crossAxisAlignment: CrossAxisAlignment.start,
                    ),
                  ),
                  const EarthRow(),
                  ...bottomObjects.map(
                    (neoWithDistance) => NEORow(
                      neoWithDistance: neoWithDistance,
                      maxDistance: maxDistance,
                      crossAxisAlignment: CrossAxisAlignment.end,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void splitNeos(
    List<NearEarthObject> neos,
    List<NEOWithDistance> topObjects,
    List<NEOWithDistance> bottomObjects,
  ) {
    NearEarthObject obj = neos.removeLast();
    topObjects.add(NEOWithDistance(neo: obj, distance: 0));
    double topDistance = obj.distance;
    obj = neos.removeLast();
    bottomObjects.add(NEOWithDistance(neo: obj, distance: 0));
    double bottomDistance = obj.distance;

    while (neos.isNotEmpty) {
      final obj = neos.removeLast();
      final newTopDistance =
          topObjects.last.distance + topObjects.last.neo.distance;
      var newBottomDistance =
          bottomObjects.last.distance + bottomObjects.last.neo.distance;

      if (newTopDistance < newBottomDistance) {
        topObjects.add(NEOWithDistance(neo: obj, distance: topDistance));
        topDistance =
            obj.distance - 20 > topDistance ? obj.distance : (topDistance + 20);
      } else {
        bottomObjects.add(NEOWithDistance(neo: obj, distance: bottomDistance));
        bottomDistance = obj.distance - 20 > bottomDistance
            ? obj.distance
            : (bottomDistance + 20);
      }
    }
  }

  Future<void> fetchImages() async {
    print('fetching neos for ${_dateController.text}');
    final uri = Uri.parse(_apiUrl(_dateController.text));
    final response = await http.get(uri);

    if (response.statusCode == 200) {
      final data = NearEarthObjectResponseDto.fromJson(
        jsonDecode(response.body) as Map<String, dynamic>,
      );

      setState(() {
        DateFormat format = DateFormat("yyyy-MMM-dd HH:mm");
        neos = [];

        data.nearEarthObjects.forEach((dateString, objects) {
          neos.addAll(objects
              .map((obj) => NearEarthObject(
                    name: obj.name,
                    distance: obj.closeApproachData[0].missDistance.kilometers,
                    size: obj.estimatedDiameter.kilometers.estimatedDiameterMax,
                    approachDate: format
                        .parse(obj.closeApproachData[0].closeApproachDateFull),
                  ))
              .toList());
        });

        neos.add(NearEarthObject(
            name: 'Moon',
            distance: 384400,
            size: 3475,
            approachDate: DateTime.now()));

        neos.sort((a, b) => b.distance.compareTo(a.distance));

        maxDistance = neos.fold(0, (value, element) {
          if (element.distance > value) {
            return element.distance.toInt();
          } else {
            return value;
          }
        });
      });
    } else {
      throw Exception('Failed to fetch images');
    }
  }

  String _apiUrl(String date) {
    return 'https://api.nasa.gov/neo/rest/v1/feed?start_date=$date&end_date=$date&api_key=DEMO_KEY';
  }
}
