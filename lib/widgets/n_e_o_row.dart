import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_nasa_app/models/n_e_o_with_distance.dart';
import 'package:intl/intl.dart';

class NEORow extends StatefulWidget {
  const NEORow({
    super.key,
    required this.maxDistance,
    required this.neoWithDistance,
    required this.crossAxisAlignment,
  });

  final int maxDistance;
  final NEOWithDistance neoWithDistance;
  final CrossAxisAlignment crossAxisAlignment;

  @override
  State<NEORow> createState() => _NEORowState();
}

class _NEORowState extends State<NEORow> with TickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 4),
    vsync: this,
  )..repeat();

  final Tween<double> turnsTween = Tween<double>(
    begin: 1,
    end: 0,
  );

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var textStyle =
        const TextStyle(fontSize: 10, height: 1, color: Colors.white);

    return SizedBox(
      height: max(
          widget.neoWithDistance.neo.name == 'Moon' ? 20 : 10,
          ((widget.neoWithDistance.neo.distance -
                      widget.neoWithDistance.distance) /
                  widget.maxDistance) *
              ((MediaQuery.of(context).size.height - 80 - 80) / 2)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: widget.crossAxisAlignment,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width / 3 - 60,
            child: Align(
              alignment: widget.neoWithDistance.neo.name == 'Moon'
                  ? Alignment.centerRight
                  : widget.crossAxisAlignment == CrossAxisAlignment.start
                      ? Alignment.topRight
                      : Alignment.bottomRight,
              child: Text(
                '${(widget.neoWithDistance.neo.distance / 1000).toStringAsFixed(0)} kkm',
                style: textStyle,
              ),
            ),
          ),
          widget.neoWithDistance.neo.name == 'Moon'
              ? Image.asset('assets/moon.png', height: 20)
              : Image.asset(
                  'assets/asteroid.gif',
                  height: 10,
                ),
          //   RotationTransition(
          //   turns: turnsTween.animate(_controller),
          //   child: Image.asset(
          //     'assets/asteroid.gif',
          //     height: 10,
          //   ),
          // ),
          SizedBox(
            width: 2 * MediaQuery.of(context).size.width / 3 - 60,
            child: Align(
              alignment: widget.neoWithDistance.neo.name == 'Moon'
                  ? Alignment.centerLeft
                  : widget.crossAxisAlignment == CrossAxisAlignment.start
                      ? Alignment.topLeft
                      : Alignment.bottomLeft,
              child: widget.neoWithDistance.neo.name == 'Moon'
                  ? Text(
                      'Moon',
                      style: textStyle,
                    )
                  : RichText(
                      text: TextSpan(
                      children: [
                        TextSpan(
                          text: '@${DateFormat('HH:mm').format(widget.neoWithDistance.neo.approachDate)}',
                          style: textStyle.copyWith(
                            color: Colors.grey,
                          )
                        ),
                        TextSpan(
                          text: '  ${widget.neoWithDistance.neo.name}',
                          style: textStyle,
                        ),
                      ],
                    )),
            ),
          ),
        ],
      ),
    );
  }
}
