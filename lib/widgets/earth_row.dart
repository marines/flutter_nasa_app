import 'package:flutter/material.dart';

class EarthRow extends StatelessWidget {
  const EarthRow({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width / 3 - 45,
            child: Align(alignment: Alignment.topRight, child: const Text('')),
          ),
          Image.asset(
            'assets/earth.png',
            height: 40,
          ),
          SizedBox(
            width: 2 * MediaQuery.of(context).size.width / 3 - 45,
            child: Text(
              'Earth',
              style: const TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}
