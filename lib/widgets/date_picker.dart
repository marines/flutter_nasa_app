import 'package:flutter/material.dart';

typedef OnSelectedCallback = void Function(String date);

class DatePicker extends StatelessWidget {
  const DatePicker({
    super.key,
    required this.dateController,
    required this.onSelected,
    required this.context,
  });

  final TextEditingController dateController;
  final OnSelectedCallback onSelected;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 170,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
        child: TextField(
          controller: dateController,
          decoration: const InputDecoration(
            contentPadding: EdgeInsets.all(0),
            border: OutlineInputBorder(),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white30, width: 1.0),
            ),
            prefixIcon: Icon(Icons.calendar_today_outlined),
          ),
          readOnly: true,
          onTap: () {
            _selectDate();
          },
        ),
      ),
    );
  }

  Future<void> _selectDate() async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.parse(dateController.text),
      firstDate: DateTime(1990),
      lastDate: DateTime.now().add(const Duration(days: 7)),
    );

    if (picked != null) {
      String date = picked.toString().split(' ')[0];
      dateController.text = date;
      onSelected(date);
    }
  }
}
