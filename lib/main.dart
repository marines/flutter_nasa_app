import 'package:flutter/material.dart';
import 'package:flutter_nasa_app/earth.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Asteroids Today',
      theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(
              seedColor: Colors.black87, brightness: Brightness.dark)),
      home: const EarthPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
