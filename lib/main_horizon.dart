import 'dart:math';

import 'package:flutter/material.dart';
import 'package:motion_sensors/motion_sensors.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  double pitch = 0.0;
  double yaw = 0.0;
  double roll = 0.0;

  @override
  void initState() {
    super.initState();
    motionSensors.absoluteOrientation.listen((AbsoluteOrientationEvent event) {
      setState(() {
        pitch = event.pitch;
        yaw = event.yaw;
        roll = event.roll;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: NetworkImage('https://picsum.photos/200/300'),
              fit: BoxFit.cover,
            ),
          ),
          child: SafeArea(
            child: Center(
              child: CustomPaint(
                painter: HorizonLine(
                  pitch: pitch,
                  yaw: yaw,
                  roll: roll,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Pitch: ${pitch * 90} degrees',
                      style: const TextStyle(
                        backgroundColor: Colors.white,
                      ),
                    ),
                    Text('Yaw: ${yaw * 180} degrees',
                        style: const TextStyle(
                          backgroundColor: Colors.white,
                        )),
                    Text('Roll: ${roll * 180} degrees',
                        style: const TextStyle(
                          backgroundColor: Colors.white,
                        )),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class HorizonLine extends CustomPainter {
  final double pitch;
  final double yaw;
  final double roll;

  HorizonLine({required this.pitch, required this.yaw, required this.roll});

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.blue
      ..strokeWidth = 2.0;

    final centerX = size.width / 2;
    final centerY = size.height / 2;

    final horizonLineLength =
        0.3 * sqrt(size.width * size.width + size.height * size.height);
    final horizonLineY = centerY + pitch * 180;

    
      canvas.drawLine(
        Offset(0, horizonLineY - roll * 180),
        Offset(size.width, horizonLineY + roll * 180),
        paint,
      );
  }

  @override
  bool shouldRepaint(HorizonLine oldDelegate) {
    return oldDelegate.pitch != pitch ||
        oldDelegate.yaw != yaw ||
        oldDelegate.roll != roll;
  }
}
