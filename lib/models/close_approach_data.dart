import 'miss_distance.dart';
import 'relative_velocity.dart';

class CloseApproachData {
  final String closeApproachDate;
  final String closeApproachDateFull;
  final int epochDateCloseApproach;
  final RelativeVelocity relativeVelocity;
  final MissDistance missDistance;
  final String orbitingBody;

  CloseApproachData({
    required this.closeApproachDate,
    required this.closeApproachDateFull,
    required this.epochDateCloseApproach,
    required this.relativeVelocity,
    required this.missDistance,
    required this.orbitingBody,
  });

  factory CloseApproachData.fromJson(Map<String, dynamic> json) {
    return CloseApproachData(
      closeApproachDate: json['close_approach_date'] as String,
      closeApproachDateFull: json['close_approach_date_full'] as String,
      epochDateCloseApproach: json['epoch_date_close_approach'] as int,
      relativeVelocity: RelativeVelocity.fromJson(
          json['relative_velocity'] as Map<String, dynamic>),
      missDistance:
          MissDistance.fromJson(json['miss_distance'] as Map<String, dynamic>),
      orbitingBody: json['orbiting_body'] as String,
    );
  }
}
