import 'package:flutter_nasa_app/models/near_earth_object.dart';

class NEOWithDistance {
  final NearEarthObject neo;
  final double distance;

  NEOWithDistance({
    required this.neo,
    required this.distance,
  });
}
