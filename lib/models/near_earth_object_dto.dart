import 'close_approach_data.dart';
import 'estimated_diameter.dart';

class NearEarthObjectDto {
  final String id;
  final String neoReferenceId;
  final String name;
  final String nasaJplUrl;
  final double absoluteMagnitudeH;
  final EstimatedDiameter estimatedDiameter;
  final bool isPotentiallyHazardousAsteroid;
  final List<CloseApproachData> closeApproachData;
  final bool isSentryObject;

  NearEarthObjectDto({
    required this.id,
    required this.neoReferenceId,
    required this.name,
    required this.nasaJplUrl,
    required this.absoluteMagnitudeH,
    required this.estimatedDiameter,
    required this.isPotentiallyHazardousAsteroid,
    required this.closeApproachData,
    required this.isSentryObject,
  });

  factory NearEarthObjectDto.fromJson(Map<String, dynamic> json) {
    return NearEarthObjectDto(
      id: json['id'] as String,
      neoReferenceId: json['neo_reference_id'] as String,
      name: json['name'] as String,
      nasaJplUrl: json['nasa_jpl_url'] as String,
      absoluteMagnitudeH: json['absolute_magnitude_h'] as double,
      estimatedDiameter: EstimatedDiameter.fromJson(
          json['estimated_diameter'] as Map<String, dynamic>),
      isPotentiallyHazardousAsteroid:
          json['is_potentially_hazardous_asteroid'] as bool,
      closeApproachData: List<CloseApproachData>.from(
          (json['close_approach_data'] as List<dynamic>).map((data) =>
              CloseApproachData.fromJson(data as Map<String, dynamic>))),
      isSentryObject: json['is_sentry_object'] as bool,
    );
  }
}
