class MissDistance {
  final double astronomical;
  final double lunar;
  final double kilometers;
  final double miles;

  MissDistance({
    required this.astronomical,
    required this.lunar,
    required this.kilometers,
    required this.miles,
  });

  factory MissDistance.fromJson(Map<String, dynamic> json) {
    return MissDistance(
      astronomical: double.parse(json['astronomical'] as String),
      lunar: double.parse(json['lunar'] as String),
      kilometers: double.parse(json['kilometers'] as String),
      miles: double.parse(json['miles'] as String),
    );
  }
}
