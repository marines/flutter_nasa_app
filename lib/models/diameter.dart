class Diameter {
  final double estimatedDiameterMin;
  final double estimatedDiameterMax;

  Diameter({
    required this.estimatedDiameterMin,
    required this.estimatedDiameterMax,
  });

  factory Diameter.fromJson(Map<String, dynamic> json) {
    return Diameter(
      estimatedDiameterMin: json['estimated_diameter_min'] as double,
      estimatedDiameterMax: json['estimated_diameter_max'] as double,
    );
  }
}
