class Links {
  final String next;
  final String prev;
  final String self;

  Links({
    required this.next,
    required this.prev,
    required this.self,
  });

  factory Links.fromJson(Map<String, dynamic> json) {
    return Links(
      next: json['next'] as String,
      prev: json['prev'] as String,
      self: json['self'] as String,
    );
  }
}
