class RelativeVelocity {
  final double kilometersPerSecond;
  final double kilometersPerHour;
  final double milesPerHour;

  RelativeVelocity({
    required this.kilometersPerSecond,
    required this.kilometersPerHour,
    required this.milesPerHour,
  });

  factory RelativeVelocity.fromJson(Map<String, dynamic> json) {
    return RelativeVelocity(
      kilometersPerSecond:
          double.parse(json['kilometers_per_second'] as String),
      kilometersPerHour: double.parse(json['kilometers_per_hour'] as String),
      milesPerHour: double.parse(json['miles_per_hour'] as String),
    );
  }
}
