import 'diameter.dart';

class EstimatedDiameter {
  final Diameter kilometers;
  final Diameter meters;
  final Diameter miles;
  final Diameter feet;

  EstimatedDiameter({
    required this.kilometers,
    required this.meters,
    required this.miles,
    required this.feet,
  });

  factory EstimatedDiameter.fromJson(Map<String, dynamic> json) {
    return EstimatedDiameter(
      kilometers: Diameter.fromJson(json['kilometers'] as Map<String, dynamic>),
      meters: Diameter.fromJson(json['meters'] as Map<String, dynamic>),
      miles: Diameter.fromJson(json['miles'] as Map<String, dynamic>),
      feet: Diameter.fromJson(json['feet'] as Map<String, dynamic>),
    );
  }
}
