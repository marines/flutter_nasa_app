class NearEarthObject {
  final String name;
  final double distance;
  final double size;
  final DateTime approachDate;

  NearEarthObject({
    required this.name,
    required this.distance,
    required this.size,
    required this.approachDate,
  });
}
