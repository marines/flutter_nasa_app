import 'links.dart';
import 'near_earth_object_dto.dart';

class NearEarthObjectResponseDto {
  final Links links;
  final int elementCount;
  final Map<String, List<NearEarthObjectDto>> nearEarthObjects;

  NearEarthObjectResponseDto({
    required this.links,
    required this.elementCount,
    required this.nearEarthObjects,
  });

  factory NearEarthObjectResponseDto.fromJson(Map<String, dynamic> json) {
    return NearEarthObjectResponseDto(
      links: Links.fromJson(json['links'] as Map<String, dynamic>),
      elementCount: json['element_count'] as int,
      nearEarthObjects:
          (json['near_earth_objects'] as Map<String, dynamic>).entries.fold(
        <String, List<NearEarthObjectDto>>{},
        (value, entry) {
          value.addAll(
            {
              entry.key: List<NearEarthObjectDto>.from(
                (entry.value as List<dynamic>).map(
                  (e) => NearEarthObjectDto.fromJson(e as Map<String, dynamic>),
                ),
              )
            },
          );

          return value;
        },
      ),
    );
  }
}
